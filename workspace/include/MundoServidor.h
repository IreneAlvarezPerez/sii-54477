// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include <signal.h>
#include "Plano.h"
#include "Socket.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"

#include "DatosMemCompartida.h"

class CMundoServidor  
{
public:
	void Init();
	CMundoServidor ();
	virtual ~CMundoServidor ();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	void RecibeComandosJugador();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	//DatosMemCompartida datos;
	//DatosMemCompartida* direccion;

	int puntos1;
	int puntos2;
	
	int fd;
	//int fd1;
	int fd2;
	int fd3;
	char* tuberia="/tmp/FIFOtenis";
	char* tuberia2="/tmp/Tuberia s-c";//envio coordenadas
	char* tuberia3="/tmp/Tuberia c-s";//envio teclas
	pthread_t thid1;
	struct sigaction accion;
	Socket conexion, comunicacion;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)


