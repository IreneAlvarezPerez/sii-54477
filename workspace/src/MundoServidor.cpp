// Como Mundo.cpp
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	if(close(fd)==-1)
	    perror("Error al cerrar la FIFO");
	//munmap(direccion, sizeof(datos));
	//close(fd2);
	//unlink(tuberia2);
	//close(fd3);
	//unlink(tuberia3);

}
void tratar_senal(int signal){ 

	if (signal ==SIGINT|| signal ==SIGTERM|| signal ==SIGPIPE){

	printf("El proceso terminó por la señal %d \n",signal);

	exit(signal);

	}

	if (signal==SIGUSR2){

	printf("El proceso terminó bien \n");

	exit(0);

	}

}

void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d;
      p->RecibeComandosJugador();
}

void CMundoServidor::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[100];
            //int f=read(fd3, cad, sizeof(cad));
           // if(f<0)perror("Error lectura tecla en MundoServidor.cpp");
           comunicacion.Receive(cad, sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera.Disminuir(0.025f);
	/*
	direccion->esfera=esfera;
	direccion->raqueta1=jugador1;
	direccion->raqueta2=jugador2;
	
	direccion->tiempo=direccion->tiempo+0.025f;
	
	switch (direccion->accion)
	{
	
	case -1:
		OnKeyboardDown('s',1,1);
		break;
		
	case 0: 
		//jugador1.velocidad.y=0;
		break;
			
	case 1:
		OnKeyboardDown('w',1,1);
		break;
		
	}
	
	switch (direccion->accion2)
	{
	
	case -1:
		jugador2.velocidad.y=-4;
		break;
	case 0: 
		//jugador2.velocidad.y=0;
		break;	
	case 1:
		jugador2.velocidad.y=4;
		break;

	
	}
	*/
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		char c[56];
		sprintf(c,"Jugador %d marca 1 punto, lleva un total de %d puntos.\n", (int)2, puntos2);
		write(fd, c, sizeof(c));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		char cd[56];
		sprintf(cd,"Jugador %d marca 1 punto, lleva un total de %d puntos.\n",(int)1, puntos1);
		write(fd, cd, sizeof(cd));
	}
	
	//Funcionalidad adicional, fin al llegar a 3 ptos
	if (puntos1==3 || puntos2==3){
	char fin[15];
	sprintf(fin,"Fin del juego");
	write(fd,fin,sizeof(fin));
	exit(0);
	}
	
	char cad[100];
sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	//int n=write(fd2,cad,sizeof(cad));
	//if(n<0) perror("Error escritura coord en MundoServidor.cpp");
	comunicacion.Send(cad, sizeof(cad));

}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
/*
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;
		//direccion->tiempo=0.0f;
		//direccion->accion2=0;
	break;
	case 'o':jugador2.velocidad.y=4;
		//direccion->tiempo=0.0f;
		//direccion->accion2=0;
	break;

	}
	*/
}

void CMundoServidor::Init()
{

	//Conexión 
	char IP[]="127.0.0.1";
	if (conexion.InitServer(IP, 8000)<0){
		perror("Error al abrir el servidor");
		exit(0);}
		
	comunicacion=conexion.Accept();
	//Conexión con el cliente
	char nom_cliente[50];
	comunicacion.Receive(nom_cliente, sizeof(nom_cliente));
	printf("%s ya se ha conectado a la partida");
	

	//Apertura s-c para envio coordenadas
	//fd2=open(tuberia2,O_WRONLY);
	//if (fd2<0) perror("Error apertura s-c en MundoServidor.cpp");
	//Apertura c-s para envio teclas
	//fd3=open(tuberia3,O_RDONLY);
	//if (fd3<0) perror("Error apertura c-s en MundoServidor.cpp");


	//Señales
	accion.sa_handler=&tratar_senal;
	accion.sa_flags=SA_RESTART;
	sigaction (SIGINT,&accion, NULL);
	sigaction (SIGTERM,&accion, NULL);
	sigaction (SIGPIPE,&accion, NULL);
	sigaction (SIGUSR2,&accion, NULL);
	
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	//Apertura tubería modo escritura
	fd = open(tuberia, O_WRONLY);
	if (fd==-1)
		perror("Error al abrir la FIFO en MundoServidor.cpp");
		
	//Thread
	pthread_create(&thid1, NULL, hilo_comandos, this);
		
	/*	
	//Memoria compartida
	struct stat info;
	void* tmp;
	fd1=open("/tmp/memoria.dat", O_RDWR|O_CREAT|O_TRUNC, 0666);
	if (fd1 == -1)
		perror("Error al abrir archivo memoria.dat en Mundo.cpp");
	write(fd1,&datos,sizeof(datos));
	fstat(fd1,&info);
	tmp = mmap(NULL,info.st_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd1,0);
	close(fd1);
	if(tmp==(void*)-1)
		perror("Error al proyectar en memoria");
	else 
	        direccion=(DatosMemCompartida*)tmp;
		
	*/
}

